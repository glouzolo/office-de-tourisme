package com.ubo.officedetourisme.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

/**
 * Entité représentant une sortie.
 * @author Abderraouf
 */

@Entity
@Setter
@Getter
public class Sortie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSortie")
    private Integer idSortie;
    @Basic(optional = false)
    @Column(name = "nom")
    private String nom;
    @Basic(optional = false)
    @Column(name = "date")
    private Date date;
    @Basic(optional = false)
    @Column(name = "heure_debut")
    private Time heureDebut;
    @Basic(optional = false)
    @Column(name = "heure_fin")
    private Time heureFin;
    @Basic(optional = false)
    @Column(name = "capacite")
    private int capacite;
    @Basic(optional = false)
    @Column(name = "prix")
    private double prix;
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSortie")
    private List<Reservation> reservationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSortie")
    private List<Option> optionList;

}
