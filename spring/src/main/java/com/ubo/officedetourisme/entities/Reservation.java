package com.ubo.officedetourisme.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.sql.Date;
import java.sql.Time;

/**
 * Entité représentant une reservation.
 * @author Abderraouf
 */
@Getter
@Setter
@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReservation")
    private Integer idReservation;
    @Basic(optional = false)
    @Column(name = "date")
    private Date date;
    @Basic(optional = false)
    @Column(name = "heure")
    private Time heure;
    @Basic(optional = false)
    @Column(name = "estDansPanier")
    private boolean estDansPanier;
    @JoinColumn(name = "idOption", referencedColumnName = "idOption")
    @OneToOne(cascade = CascadeType.ALL)
    private Option idOption;
    @JoinColumn(name = "idSortie", referencedColumnName = "idSortie")
    @ManyToOne(optional = false)
    private Sortie idSortie;
    @JoinColumn(name = "idUtilisateur", referencedColumnName = "idUtilisateur")
    @ManyToOne(optional = false)
    private Utilisateur idUtilisateur;

}
