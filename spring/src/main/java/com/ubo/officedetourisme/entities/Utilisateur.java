package com.ubo.officedetourisme.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.List;
/**
 * Class représentant une entité utilisateur.
 * @author Abderraouf
 */
@Entity
@Setter
@Getter

/**
 Entité représentant un utilisateur.
 */
public class Utilisateur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUtilisateur")
    private Integer idUtilisateur;
    @Column(name = "genre")
    private String genre;
    @Column(name = "nom")
    private String nom;
    @Column(name = "prenom")
    private String prenom;
    @Basic(optional = false)
    @Column(name = "login")
    private String login;
    @Basic(optional = false)
    @Column(name = "mot_de_passe")
    private String motDePasse;
    @Column(name = "adresse_mail")
    private String adresseMail;
    @Column(name = "adresse")
    private String adresse;
    @Column(name = "ville")
    private String ville;
    @Basic(optional = false)
    @Column(name = "estAdministrateur")
    private boolean estAdministrateur;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUtilisateur")
    private List<Reservation> reservationList;

}
