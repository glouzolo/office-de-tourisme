package com.ubo.officedetourisme.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


import java.util.List;

/**
 * Entité représentant une option.
 * @author Abderraouf
 */
@Entity
@Setter
@Getter

public class Option {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idOption")
    private Integer idOption;
    @Basic(optional = false)
    @Column(name = "nom")
    private String nom;
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idOption")
    private List<Reservation> reservationList;
    @JoinColumn(name = "idSortie", referencedColumnName = "idSortie")
    @ManyToOne(optional = false)
    private Sortie idSortie;
}
