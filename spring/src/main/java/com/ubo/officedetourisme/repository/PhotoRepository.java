package com.ubo.officedetourisme.repository;

import com.ubo.officedetourisme.dto.Photo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Abderraouf
 */

/**
 Interface de répertoire pour les photos utilisant MongoDB.
 Cette interface étend l'interface MongoRepository pour fournir des méthodes
 de base pour la gestion des opérations CRUD (création, lecture, mise à jour et suppression) sur les photos.
 Les opérations sont effectuées en utilisant la clé primaire de type String.
 */
@Repository
public interface PhotoRepository extends MongoRepository<Photo, String> {

}
