package com.ubo.officedetourisme.repository;

import com.ubo.officedetourisme.entities.Reservation;
import jakarta.transaction.Transactional;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Abderraouf
 */

/**
 Cette interface est un repository pour l'entité Reservation.
 Elle hérite de JpaRepository, ce qui lui permet d'utiliser les méthodes CRUD (Create, Read, Update, Delete) pour l'entité Reservation.
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    /**
     * Cette méthode supprime automatiquement toutes les réservations dans le panier de l'utilisateur dont la date et l'heure sont antérieures à la date et l'heure actuelles en utilisant une requête SQL personnalisée.
     * Seules les réservations ayant la valeur "estDansPanier" à "true" seront supprimées.
     * @throws DataAccessException s'il y a une erreur lors de l'exécution de la requête
     */

    @Query("DELETE FROM Reservation r WHERE r.estDansPanier = true AND CONCAT(r.date, ' ', r.heure) < CURRENT_TIMESTAMP")
    void deleteExpiredReservations();

}