package com.ubo.officedetourisme.repository;


import com.ubo.officedetourisme.dto.CommentaireDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Abderraouf
 */

/**
 Cette interface est un repository pour l'entité CommentaireDto.
 Elle hérite de JpaRepository, ce qui lui permet d'utiliser les méthodes CRUD (Create, Read, Update, Delete) pour l'entité CommentaireDto.
 */
@Repository
@Component
public interface CommentaireRepository extends MongoRepository<CommentaireDto, String> {
}