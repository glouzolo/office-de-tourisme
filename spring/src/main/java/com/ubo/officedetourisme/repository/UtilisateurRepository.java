package com.ubo.officedetourisme.repository;

import com.ubo.officedetourisme.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Abderraouf
 */

/**
 Cette interface est un repository pour l'entité Utilisateur.
 Elle hérite de JpaRepository, ce qui lui permet d'utiliser les méthodes CRUD (Create, Read, Update, Delete) pour l'entité Utilisateur.
 Elle contient également une méthode personnalisée findByLogin, qui utilise une requête JPQL pour récupérer un utilisateur en fonction de son login.
 */

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {
    /**
    Récupère un utilisateur en fonction de son login.
    @param login le login de l'utilisateur à récupérer.
    @return l'utilisateur correspondant, ou null s'il n'existe pas.
    */
    @Query("SELECT u FROM Utilisateur u WHERE u.login = :login LIMIT 1")
    Utilisateur findByLogin(@Param("login") String login);
}