package com.ubo.officedetourisme.repository;

import com.ubo.officedetourisme.entities.Sortie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Abderraouf
 */

/**
 Cette interface est un repository pour l'entité Sortie.
 Elle hérite de JpaRepository, ce qui lui permet d'utiliser les méthodes CRUD (Create, Read, Update, Delete) pour l'entité Sortie.
 */
@Repository
public interface SortieRepository extends JpaRepository<Sortie, Integer> {
}