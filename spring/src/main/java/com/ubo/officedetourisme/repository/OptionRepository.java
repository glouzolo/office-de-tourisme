package com.ubo.officedetourisme.repository;

import com.ubo.officedetourisme.entities.Option;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Abderraouf
 */

/**
 Cette interface est un repository pour l'entité Option.
 Elle hérite de JpaRepository, ce qui lui permet d'utiliser les méthodes CRUD (Create, Read, Update, Delete) pour l'entité Option.
 */
@Repository
public interface OptionRepository extends JpaRepository<Option, Integer> {
    }