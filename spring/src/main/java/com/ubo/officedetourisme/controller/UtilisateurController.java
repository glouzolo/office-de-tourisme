package com.ubo.officedetourisme.controller;


import com.ubo.officedetourisme.dto.LoginRequest;
import com.ubo.officedetourisme.dto.UtilisateurDto;
import com.ubo.officedetourisme.services.UtilisateurService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Contrôleur pour les opérations sur les utilisateurs.
 * @author Abderraouf
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/utilisateurs")
public class UtilisateurController {

    private final UtilisateurService utilisateurService;

    /**
     Le contrôleur pour les opérations liées à l'utilisateur.
     @param utilisateurService le service de gestion des utilisateurs
     */
    public UtilisateurController(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    /**
     Endpoint pour sauvegarder un utilisateur
     @param utilisateurDto les informations de l'utilisateur à sauvegarder
     @return ResponseEntity contenant l'utilisateur sauvegardé
     @throws NoSuchAlgorithmException si une erreur survient lors de l'encodage du mot de passe de l'utilisateur
     */
    @PostMapping
    public ResponseEntity<UtilisateurDto> saveUtilisateur(@RequestBody UtilisateurDto utilisateurDto) throws NoSuchAlgorithmException {
        UtilisateurDto savedUtilisateur = utilisateurService.saveUtilisateur(utilisateurDto);
        return ResponseEntity.ok(savedUtilisateur);
    }

    /**
     Endpoint pour récupérer un utilisateur par son identifiant
     @param utilisateurId l'identifiant de l'utilisateur à récupérer
     @return ResponseEntity contenant l'utilisateur demandé
     */
    @GetMapping("/{id}")
    public ResponseEntity<UtilisateurDto> getUtilisateurById(@PathVariable("id") int utilisateurId) {
        UtilisateurDto utilisateur = utilisateurService.getUtilisateurById(utilisateurId);
        return ResponseEntity.ok(utilisateur);
    }

    /**
     Endpoint pour supprimer un utilisateur par son identifiant
     @param utilisateurId l'identifiant de l'utilisateur à supprimer
     @return ResponseEntity indiquant si la suppression s'est bien déroulée ou non
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<UtilisateurDto> deleteUtilisateur(@PathVariable("id") int utilisateurId) {
        boolean deleted = utilisateurService.deleteUtilisateur(utilisateurId);
        if (deleted) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     Endpoint pour récupérer tous les utilisateurs
     @return ResponseEntity contenant la liste de tous les utilisateurs
     */
    @GetMapping
    public ResponseEntity<List<UtilisateurDto>> getAllUtilisateurs() {
        List<UtilisateurDto> utilisateurs = utilisateurService.getAllUtilisateurs();
        return ResponseEntity.ok(utilisateurs);
    }

    /**
     Endpoint pour mettre à jour un utilisateur par son identifiant
     @param utilisateurId l'identifiant de l'utilisateur à mettre à jour
     @param utilisateurDto les nouvelles informations de l'utilisateur
     @return ResponseEntity contenant l'utilisateur mis à jour
     @throws NoSuchAlgorithmException si une erreur survient lors de l'encodage du nouveau mot de passe de l'utilisateur
     */
    @PutMapping("/{id}")
    public ResponseEntity<UtilisateurDto> updateUtilisateur(@PathVariable("id") int utilisateurId, @RequestBody UtilisateurDto utilisateurDto) throws NoSuchAlgorithmException {
        UtilisateurDto updatedUtilisateur = utilisateurService.updateUtilisateur(utilisateurId, utilisateurDto);
        return ResponseEntity.ok(updatedUtilisateur);
    }

    /**
     Endpoint pour connecter un utilisateur
     @param loginRequest les informations de connexion de l'utilisateur
     @return ResponseEntity contenant l'utilisateur connecté ou une réponse 404 si l'utilisateur n'existe pas
     @throws NoSuchAlgorithmException si une erreur survient lors de la comparaison des mots de passe
     */
    @PostMapping("/login")
    public ResponseEntity<UtilisateurDto> login(@RequestBody LoginRequest loginRequest) throws NoSuchAlgorithmException {
        UtilisateurDto utilisateurDto = utilisateurService.loginUtilisateur(loginRequest);
        if (utilisateurDto==null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(utilisateurDto);
    }


}

