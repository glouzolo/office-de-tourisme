package com.ubo.officedetourisme.controller;

import com.ubo.officedetourisme.dto.ReservationDto;
import com.ubo.officedetourisme.services.Impl.ReservationServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Contrôleur pour les opérations sur les reservations.
 * @author Abderraouf
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/reservations")

public class ReservationController {

    private final ReservationServiceImpl reservationService;

    public ReservationController(ReservationServiceImpl reservationService) {
        this.reservationService = reservationService;
    }

    /**
     * Crée une nouvelle réservation à partir des informations fournies.
     * @param reservationDto L'objet ReservationDto contenant les informations de la réservation à créer.
     * @return Un objet ResponseEntity contenant l'objet ReservationDto créé et le code de statut HTTP correspondant.
     */
    @PostMapping
    public ResponseEntity<ReservationDto> saveReservation(@RequestBody ReservationDto reservationDto) {
        ReservationDto savedReservation = reservationService.saveReservation(reservationDto);
        return ResponseEntity.ok(savedReservation);
    }
    /**
     * Récupère une réservation à partir de son ID.
     * @param reservationId L'ID de la réservation à récupérer.
     * @return Un objet ResponseEntity contenant l'objet ReservationDto correspondant et le code de statut HTTP correspondant.
     */
    @GetMapping("/{id}")
    public ResponseEntity<ReservationDto> getReservationById(@PathVariable("id") int reservationId) {
        ReservationDto reservation = reservationService.getReservationById(reservationId);
        return ResponseEntity.ok(reservation);
    }
    /**
     * Supprime une réservation à partir de son ID.
     * @param reservationId L'ID de la réservation à supprimer.
     * @return Un objet ResponseEntity avec le code de statut HTTP correspondant.
     *         Si la réservation est supprimée avec succès, le code sera "204 No Content".
     *         Sinon, si la réservation n'est pas trouvée, le code sera "404 Not Found".
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteReservation(@PathVariable("id") int reservationId) {
        boolean deleted = reservationService.deleteReservation(reservationId);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    /**
     * Récupère la liste de toutes les réservations.
     * @return Un objet ResponseEntity contenant la liste de toutes les réservations et le code de statut HTTP correspondant.
     */
    @GetMapping
    public ResponseEntity<List<ReservationDto>> getAllReservations() {
        List<ReservationDto> reservations = reservationService.getAllReservations();
        return ResponseEntity.ok(reservations);
    }

    /**
     * Met à jour une réservation à partir de son ID et des informations fournies.
     * @param reservationId L'ID de la réservation à mettre à jour.
     * @param reservationDto L'objet ReservationDto contenant les informations de la réservation mises à jour.
     * @return Un objet ResponseEntity contenant l'objet ReservationDto mis à jour et le code de statut HTTP correspondant.
     */
    @PutMapping("/{id}")
    public ResponseEntity<ReservationDto> updateReservation(@PathVariable("id") int reservationId, @RequestBody ReservationDto reservationDto) {
        ReservationDto updatedReservation = reservationService.updateReservation(reservationId, reservationDto);
        return ResponseEntity.ok(updatedReservation);
    }


}
