package com.ubo.officedetourisme.controller;


import com.ubo.officedetourisme.dto.SortieDto;
import com.ubo.officedetourisme.services.Impl.SortieServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * Contrôleur pour les opérations sur les sorties.
 * @author Abderraouf
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/sorties")
public class SortieController {

    private final SortieServiceImpl sortieService;

    public SortieController(SortieServiceImpl sortieService) {
        this.sortieService = sortieService;
    }

    /**
     * Enregistre une sortie en base de données à partir d'un objet SortieDto reçu en entrée.
     * @param sortieDto l'objet SortieDto contenant les informations de la sortie à enregistrer
     * @return l'objet SortieDto enregistré en base de données
     */
    @PostMapping
    public SortieDto saveSortie(@RequestBody SortieDto sortieDto) {
        return sortieService.saveSortie(sortieDto);
    }

    /**
     * Récupère une sortie de la base de données à partir de son identifiant.
     * @param sortieId l'identifiant de la sortie à récupérer
     * @return l'objet SortieDto correspondant à l'identifiant donné
     */
    @GetMapping("/{sortieId}")
    public SortieDto getSortieById(@PathVariable int sortieId) {
        return sortieService.getSortieById(sortieId);
    }

    /**
     * Supprime une sortie de la base de données à partir de son identifiant.
     * @param sortieId l'identifiant de la sortie à supprimer
     * @return true si la sortie a été supprimée avec succès, false sinon
     */
    @DeleteMapping("/{sortieId}")
    public boolean deleteSortie(@PathVariable int sortieId) {
        return sortieService.deleteSortie(sortieId);
    }

    /**
     * Récupère la liste de toutes les sorties enregistrées en base de données.
     * @return la liste de toutes les sorties enregistrées en base de données
     */
    @GetMapping
    public List<SortieDto> getAllSorties() {
        return sortieService.getAllSorties();
    }

    /**
     * Met à jour une sortie en base de données à partir de son identifiant et d'un objet SortieDto reçu en entrée.
     * @param sortieId l'identifiant de la sortie à mettre à jour
     * @param sortieDto l'objet SortieDto contenant les nouvelles informations de la sortie à mettre à jour
     * @return l'objet SortieDto mis à jour en base de données
     */
    @PutMapping("/{sortieId}")
    public SortieDto updateSortie(@PathVariable int sortieId, @RequestBody SortieDto sortieDto) {
        return sortieService.updateSortie(sortieId, sortieDto);
    }

}