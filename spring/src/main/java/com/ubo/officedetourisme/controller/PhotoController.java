package com.ubo.officedetourisme.controller;


import com.ubo.officedetourisme.dto.Photo;
import com.ubo.officedetourisme.services.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
/**
 *Contrôleur pour les opérations sur les photos.
 * @author Abderraouf
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/photos")
public class PhotoController {

    @Autowired
    private PhotoService fileService;

    /**
     Upload un fichier.
     @param file le fichier à uploader
     @return une réponse HTTP avec les informations sur le fichier ajouté
     @throws IOException si une erreur survient lors de l'upload
     */
    @PostMapping("/upload")
    public ResponseEntity<?> upload(@RequestParam("file")MultipartFile file,
                                    @RequestParam("idSortie") String idSortie,
                                    @RequestParam("idUtilisateur") String idUtilisateur) throws IOException {
        return new ResponseEntity<>(fileService.addFile(file, idSortie, idUtilisateur), HttpStatus.OK);
    }
    /**
     Télécharge un fichier par son identifiant.
     //@param id l'identifiant du fichier à télécharger
     @return une réponse HTTP avec le fichier téléchargé ou une réponse HTTP NOT FOUND si le fichier n'existe pas
     @throws IOException si une erreur survient lors du téléchargement
     */
    /*@GetMapping("/download/{id}")
    public ResponseEntity<ByteArrayResource> download(@PathVariable String id) throws IOException {
        Photo loadFile = fileService.downloadFile(id);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(loadFile.getFileType() ))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + loadFile.getFilename() + "\"")
                .body(new ByteArrayResource(loadFile.getFile()));
    }*/
    @GetMapping("/download")
    public ResponseEntity<ByteArrayResource> download(@RequestParam("idSortie") String idSortie, @RequestParam("idUtilisateur") String idUtilisateur) throws IOException {
        Photo loadFile = fileService.downloadFile(idSortie, idUtilisateur);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(loadFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + loadFile.getFilename() + "\"")
                .body(new ByteArrayResource(loadFile.getFile()));
    }

}
