package com.ubo.officedetourisme.controller;

import com.ubo.officedetourisme.dto.OptionDto;
import com.ubo.officedetourisme.services.OptionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 *Contrôleur pour les opérations sur les options.
 * @author Abderraouf
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/options")
public class OptionController {

    private final OptionService optionService;

    public OptionController(OptionService optionService) {
        this.optionService = optionService;
    }

    /**
     Récupère toutes les options.
     @return la liste de toutes les options
     */
    @GetMapping
    public List<OptionDto> getOptions() {
        return optionService.getAllOptions();
    }

    /**
     Récupère une option par son identifiant.
     @param id l'identifiant de l'option à récupérer
     @return l'option correspondant à l'identifiant donné
     */
    @GetMapping("/{id}")
    public OptionDto getOption(@PathVariable int id){
        return optionService.getOptionById(id);
    }

    /**
     Enregistre une nouvelle option.
     @param optionDto les informations de l'option à ajouter
     @return l'option ajoutée
     */
    @PostMapping
    public OptionDto saveOption(final @RequestBody OptionDto optionDto){
        return optionService.saveOption(optionDto);
    }

    /**
     Supprime une option par son identifiant.
     @param id l'identifiant de l'option à supprimer
     @return true si l'option a été supprimée, false sinon
     */
    @DeleteMapping("/{id}")
    public Boolean deleteOption(@PathVariable int id){
        return optionService.deleteOption(id);
    }

    /**
     Met à jour une option par son identifiant.
     @param id l'identifiant de l'option à mettre à jour
     @param optionDto les nouvelles informations de l'option
     @return l'option mise à jour
     */
    @PutMapping("/{id}")
    public OptionDto updateSoiree(@PathVariable int id, final @RequestBody OptionDto optionDto) {
        return optionService.updateOption(id, optionDto);
    }




}
