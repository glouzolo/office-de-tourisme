package com.ubo.officedetourisme.controller;

import com.ubo.officedetourisme.dto.CommentaireDto;
import com.ubo.officedetourisme.services.CommentaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.List;

/**
 * Contrôleur pour les opérations sur les commentaires.
 * @author Abderraouf
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/commentaires")
public class CommentaireController {

    private final CommentaireService commentaireService;

    @Autowired
    public CommentaireController(CommentaireService commentaireService) {
        this.commentaireService = commentaireService;
    }

    /**
     Enregistre un nouveau commentaire.
     @param commentaireDto le commentaire à enregistrer
     @return une réponse HTTP avec le commentaire enregistré
     @throws IOException si une erreur survient lors de l'enregistrement
     */
    @PostMapping
    public ResponseEntity<CommentaireDto> createCommentaire(@RequestBody CommentaireDto commentaireDto) throws IOException {
        CommentaireDto savedCommentaireDto = commentaireService.saveCommentaire(commentaireDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedCommentaireDto);
    }
    /**
     Récupère un commentaire par son identifiant.
     @param id l'identifiant du commentaire à récupérer
     @return une réponse HTTP avec le commentaire correspondant ou une réponse HTTP NOT FOUND si le commentaire n'existe pas
     */
    @GetMapping("/{id}")
    public ResponseEntity<CommentaireDto> getCommentaireById(@PathVariable String id) {
        CommentaireDto commentaireDto = commentaireService.getCommentaireById(id);
        if (commentaireDto == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(commentaireDto);
    }
    /**

     Récupère tous les commentaires.
     @return une réponse HTTP avec la liste de tous les commentaires
     */
    @GetMapping
    public ResponseEntity<List<CommentaireDto>> getAllCommentaires() {
        List<CommentaireDto> commentaireDtoList = commentaireService.getAllCommentaires();
        return ResponseEntity.ok(commentaireDtoList);
    }
    /**
     Met à jour un commentaire existant.
     @param id l'identifiant du commentaire à mettre à jour
     @param commentaireDto les nouvelles données du commentaire
     @return une réponse HTTP avec le commentaire mis à jour ou une réponse HTTP NOT FOUND si le commentaire n'existe pas
     */
    @PutMapping("/{id}")
    public ResponseEntity<CommentaireDto> updateCommentaire(@PathVariable String id, @RequestBody CommentaireDto commentaireDto) {
        CommentaireDto updatedCommentaireDto = commentaireService.updateCommentaire(id, commentaireDto);
        return ResponseEntity.ok(updatedCommentaireDto);
    }
    /**
     Supprime un commentaire existant.
     @param id l'identifiant du commentaire à supprimer
     @return une réponse HTTP NO CONTENT si le commentaire a été supprimé ou une réponse HTTP NOT FOUND si le commentaire n'existe pas
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCommentaire(@PathVariable String id) {
        boolean isDeleted = commentaireService.deleteCommentaire(id);
        if (isDeleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

}
