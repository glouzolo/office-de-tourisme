package com.ubo.officedetourisme.services;

import com.ubo.officedetourisme.dto.SortieDto;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 *
 * @author Abderraouf
 */
@Service
public interface SortieService {

    /**
     * Enregistre un nouvel objet ReservationDto.
     * @param sortieDto L'objet ReservationDto à enregistrer.
     * @return L'objet SortieDto enregistré.
     */
    SortieDto saveSortie(SortieDto sortieDto);

    /**
     * Récupère un objet ReservationDto par son ID.
     * @param sortieId L'ID de l'objet SortieDto à récupérer.
     * @return L'objet SortieDto récupéré.
     */
    SortieDto getSortieById(int sortieId);

    /**
     * Supprime un objet SortieDto par son ID.
     * @return Vrai si l'objet SortieDto a été supprimé avec succès, faux sinon.
     */
    boolean deleteSortie(int sortieId);

    /**
     * Récupère une liste de tous les objets SortieDto.
     * @return La liste des objets SortieDto.
     */
    List<SortieDto> getAllSorties();

    /**
     * Met à jour un objet ReservationDto par son ID.
     * @param sortieId L'ID de l'objet SortieDto à mettre à jour.
     * @param sortieDto L'objet SortieDto mis à jour.
     * @return L'objet SortieDto mis à jour.
     */
    SortieDto updateSortie(int sortieId, SortieDto sortieDto);
}
