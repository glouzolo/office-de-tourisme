package com.ubo.officedetourisme.services.Impl;

import com.ubo.officedetourisme.dto.ReservationDto;
import com.ubo.officedetourisme.entities.Option;
import com.ubo.officedetourisme.entities.Reservation;
import com.ubo.officedetourisme.entities.Sortie;
import com.ubo.officedetourisme.entities.Utilisateur;
import com.ubo.officedetourisme.repository.ReservationRepository;
import com.ubo.officedetourisme.repository.SortieRepository;
import com.ubo.officedetourisme.services.ReservationService;
import org.springframework.dao.DataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 *
 * @author Abderraouf
 */
@Service("ReservationService")
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    private final SortieRepository sortieRepository;

    String notExsitStart = "la reservation avec l'id :" ;
    String notExsitEnd = " n'existe pas" ;

    public ReservationServiceImpl(ReservationRepository reservationRepository, SortieRepository sortieRepository) {
        this.reservationRepository = reservationRepository;
        this.sortieRepository = sortieRepository;
    }
    /**
     Enregistre une nouvelle réservation dans la base de données et met à jour la capacité de la sortie correspondante.
     Vérifie également si l'utilisateur a déjà réservé une sortie pour la même date et heure.
     @param reservationDto l'objet ReservationDto à enregistrer
     @return l'objet ReservationDto enregistré avec l'ID généré
     @throws IllegalArgumentException si l'utilisateur a déjà réservé une sortie pour la même date et heure
     */
    public ReservationDto saveReservation(ReservationDto reservationDto) {
        Optional<Sortie> optionalSortie = sortieRepository.findById(reservationDto.getIdSortie());
        boolean present = optionalSortie.isPresent();
        // Pense à faire une methode pour récupérer les reservation de l'utilisateur de la date et de l'heure au lieu de getAllReservations()
        // Vérifie si une sortie est présente pour la réservation
        List<ReservationDto> reservationList = getAllReservations();
        for (ReservationDto reservationDto1 : reservationList)
            /*
            Vérifie si une réservation existe déjà pour l'utilisateur et la même date et heure que celle que l'utilisateur essaie de réserver.
            Si une sortie est présente pour la réservation et que l'heure de fin de la sortie est après l'heure de la réservation en cours,
            cela signifie qu'il y a un chevauchement d'horaires et qu'il n'est pas possible de faire la réservation.
             */
            if(reservationDto1.getIdUtilisateur()==reservationDto.getIdUtilisateur() &&
               reservationDto1.getDate().equals(reservationDto.getDate()) && present &&
                    (reservationDto1.getHeure().equals(reservationDto.getHeure()) ||
                            optionalSortie.get().getHeureFin().after(reservationDto.getHeure())))
                throw new IllegalArgumentException("Vous ne pouvez pas réserver car vous avez une reservation a ce jour et heure..");


        reservationDto.setEstDansPanier(true);
        return toDto(reservationRepository.save(toEntity(reservationDto)));
    }

    /**
     Récupère une réservation par son identifiant.
     @param reservationId l'identifiant de la réservation à récupérer
     @return l'objet ReservationDto correspondant à l'identifiant donné ou un nouvel objet ReservationDto vide si la réservation n'existe pas
     */
    @Override
    public ReservationDto getReservationById(int reservationId) {
       Optional<Reservation> reservation = reservationRepository.findById(reservationId);
        if(reservation.isPresent()) {
            return toDto(reservation.get());
        }
        return new ReservationDto();
    }
    /**
     Supprime une réservation spécifique en utilisant l'ID de la réservation.
     @param reservationId l'ID de la réservation à supprimer
     @return true si la suppression a réussi, false sinon
     @throws IllegalArgumentException si la réservation avec l'ID donné n'existe pas
     */
    @Override
    public boolean deleteReservation(int reservationId) {
        if(!reservationRepository.existsById(reservationId))
            throw new IllegalArgumentException(notExsitStart + reservationId + notExsitEnd);

        Optional<Sortie> optionalSortie = sortieRepository.findById(getReservationById(reservationId).getIdSortie());
        boolean present = optionalSortie.isPresent();
        Date today = (Date) Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        //verifier si la reservation existe et assurer que la reservation n'est pas déjà passé avant de la supprimer
        if (present && optionalSortie.get().getDate().after(today)) {
            Sortie sortie = optionalSortie.get();
            sortie.setCapacite(sortie.getCapacite() + 1);
        }
        reservationRepository.deleteById(reservationId);
        return true;
    }
    /**
     Renvoie une liste de toutes les réservations stockées dans la base de données.
     @return La liste des réservations stockées dans la base de données.
     */
    @Override
    public List<ReservationDto> getAllReservations() {
        return toDto(reservationRepository.findAll());
    }

    /**
     Met à jour la réservation avec l'ID spécifié avec les informations fournies dans l'objet ReservationDto.
     @param reservationId l'ID de la réservation à mettre à jour
     @param reservationDto les nouvelles informations de la réservation
     @return l'objet ReservationDto de la réservation mise à jour
     @throws IllegalArgumentException si la réservation avec l'ID spécifié n'existe pas
     */
    @Override
    public ReservationDto updateReservation(int reservationId, ReservationDto reservationDto) {
        ReservationDto reservation = getReservationById(reservationId);
        if(reservation==null)
            throw new IllegalArgumentException(notExsitStart + reservationId + notExsitEnd);
        reservation.setEstDansPanier(true);
        reservation.setIdOption(reservationDto.getIdOption());

        Optional<Sortie> optionalSortie = sortieRepository.findById(reservationDto.getIdSortie());
        boolean present = optionalSortie.isPresent();
        if (present) {
            Sortie sortie = optionalSortie.get();
            sortie.setCapacite(sortie.getCapacite() - 1);
        }
        return toDto(reservationRepository.save(toEntity(reservation)));
    }
    /**
     Cette méthode permet de convertir un objet ReservationDto en objet Reservation.
     @param dto l'objet ReservationDto à convertir
     @return l'objet Reservation correspondant à l'objet ReservationDto
     */
    public Reservation toEntity(ReservationDto dto) {
        Reservation entity = new Reservation();
        entity.setIdReservation(dto.getIdReservation());
        entity.setDate(dto.getDate());
        entity.setHeure(dto.getHeure());
        entity.setEstDansPanier(dto.isEstDansPanier());
        // create new GroupeEntity and set its ID
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(dto.getIdUtilisateur());
        entity.setIdUtilisateur(utilisateur);
        // cree une nouvelle option et set son id si l'option n'est pas null
        if(dto.getIdOption()!=null) {
            Option option = new Option();
            option.setIdOption(dto.getIdOption());
            entity.setIdOption(option);
        }
        // create new Sortie and set its ID
        Sortie sortie = new Sortie();
        sortie.setIdSortie(dto.getIdSortie());
        entity.setIdSortie(sortie);

        return entity;
    }
    /**
     Cette méthode permet de convertir un objet Reservation en objet ReservationDto.
     @param reservation l'objet Reservation à convertir
     @return l'objet ReservationDto correspondant à l'objet Reservation
     */
    public ReservationDto toDto(Reservation reservation) {
        ReservationDto dto = new ReservationDto();
        dto.setIdReservation(reservation.getIdReservation());
        dto.setIdSortie(reservation.getIdSortie().getIdSortie());
        dto.setIdUtilisateur(reservation.getIdUtilisateur().getIdUtilisateur());
        dto.setDate(reservation.getDate());
        dto.setHeure(reservation.getHeure());
        dto.setEstDansPanier(reservation.isEstDansPanier());
        if(reservation.getIdOption()!=null) {
            dto.setIdOption(reservation.getIdOption().getIdOption());
        }



        return dto;
    }
    /**
     Cette méthode permet de convertir une liste d'objets ReservationDto en une liste d'objets Reservation.
     @param dtoList la liste d'objets ReservationDto à convertir
     @return la liste d'objets Reservation correspondant à la liste d'objets ReservationDto
     */
    public List<Reservation> toEntity(List<ReservationDto> dtoList) {
        if(dtoList.isEmpty()) return new ArrayList<>();
        List<Reservation> reservationList = new ArrayList<>();
        for(ReservationDto reservationDto : dtoList)
            if(reservationDto != null)
                reservationList.add(toEntity(reservationDto));
        return reservationList;
    }
    /**
     Cette méthode permet de convertir une liste d'objets Reservation en une liste d'objets ReservationDto.
     @param entityList la liste d'objets Reservation à convertir
     @return la liste d'objets ReservationDto correspondant à la liste d'objets Reservation
     */
    public List<ReservationDto> toDto(List<Reservation> entityList) {
        if(entityList.isEmpty()) return new ArrayList<>();
        List<ReservationDto> reservationList = new ArrayList<>();
        for(Reservation reservation : entityList)
            if(reservation != null)
                reservationList.add(toDto(reservation));
        return reservationList;
    }

    /**
     * Cette méthode est exécutée à minuit tous les jours pour supprimer toutes les réservations qui sont dans le panier de l'utilisateur
     * dont la date et l'heure sont antérieures à la date et l'heure actuelles.
     * La suppression est effectuée automatiquement en utilisant la méthode "DeletePanierExpire()" du repository ReservationRepository.
     * @throws DataAccessException s'il y a une erreur lors de l'exécution de la requête
     */
    @Scheduled(cron = "0 0 * * * *") // execute at midnight every day
    public void deleteExpiredReservations() {
        reservationRepository.deleteExpiredReservations();
    }

}
