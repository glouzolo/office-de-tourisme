package com.ubo.officedetourisme.services;

import com.ubo.officedetourisme.dto.OptionDto;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 *
 * @author Abderraouf
 */
@Service
public interface OptionService {

    /**
     * Enregistre un nouvel objet OptionDto.
     * @param optionDto L'objet OptionDto à enregistrer.
     * @return L'objet OptionDto enregistré.
     */
    OptionDto saveOption(OptionDto optionDto);

    /**
     * Récupère un objet OptionDto par son ID.
     * @param optionId L'ID de l'objet OptionDto à récupérer.
     * @return L'objet OptionDto récupéré.
     */
    OptionDto getOptionById(int optionId);

    /**
     * Supprime un objet OptionDto par son ID.
     * @param optionId L'ID de l'objet OptionDto à supprimer.
     * @return Vrai si l'objet OptionDto a été supprimé avec succès, faux sinon.
     */
    boolean deleteOption(int optionId);

    /**
     * Récupère une liste de tous les objets OptionDto.
     * @return La liste des objets OptionDto.
     */
    List<OptionDto> getAllOptions();

    /**
     * Met à jour un objet OptionDto par son ID.
     * @param optionId L'ID de l'objet OptionDto à mettre à jour.
     * @param optionDto L'objet OptionDto mis à jour.
     * @return L'objet OptionDto mis à jour.
     */
    OptionDto updateOption(int optionId, OptionDto optionDto);

}
