package com.ubo.officedetourisme.services.Impl;

import com.ubo.officedetourisme.dto.OptionDto;
import com.ubo.officedetourisme.dto.ReservationDto;
import com.ubo.officedetourisme.dto.SortieDto;
import com.ubo.officedetourisme.entities.Sortie;
import com.ubo.officedetourisme.repository.SortieRepository;
import com.ubo.officedetourisme.services.SortieService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 *
 * @author Abderraouf
 */
@Service("SortieService")
public class SortieServiceImpl implements SortieService {

    private final OptionServiceImpl optionService;
    private final ReservationServiceImpl reservationService;
    private final SortieRepository sortieRepository;
    String notExsitStart = "la sortie avec l'id :" ;
    String notExsitEnd = " n'existe pas" ;

    public SortieServiceImpl(OptionServiceImpl optionService, ReservationServiceImpl reservationService, SortieRepository sortieRepository) {
        this.optionService = optionService;
        this.reservationService = reservationService;
        this.sortieRepository = sortieRepository;
    }
    /**

     Enregistre une nouvelle sortie.
     @param sortieDto l'objet SortieDto à enregistrer.
     @return l'objet SortieDto enregistré.
     */
    @Override
    public SortieDto saveSortie(SortieDto sortieDto) {
        Sortie sortie = new Sortie();
        BeanUtils.copyProperties(sortieDto, sortie);
        Sortie savedSortie = sortieRepository.save(sortie);
        SortieDto savedSortieDto = new SortieDto();
        BeanUtils.copyProperties(savedSortie, savedSortieDto);
        return savedSortieDto;
    }

    /**
     Obtient une sortie en fonction de son ID.
     @param sortieId ID de la sortie à récupérer
     @return la sortie correspondante à l'ID fourni
     @throws EntityNotFoundException si la sortie n'existe pas dans la base de données
     */
    @Override
    public SortieDto getSortieById(int sortieId) {
        Optional<Sortie> sortie = sortieRepository.findById(sortieId);
        if(sortie.isPresent())
            return toDto(sortie.get());
        throw new EntityNotFoundException(notExsitStart + sortieId + notExsitEnd);
    }

    /**
     Supprime une sortie existante à partir de son identifiant.
     @param sortieId l'identifiant de la sortie à supprimer
     @return true si la suppression s'est bien déroulée, sinon false
     @throws EntityNotFoundException si la sortie avec l'identifiant spécifié n'existe pas
     @throws IllegalArgumentException si la sortie est déjà passée, si la date fournie est antérieure à la date d'aujourd'hui,
     ou s'il y a une/des réservation(s) associée(s) à la sortie
     */
    @Override
    public boolean deleteSortie(int sortieId) {
        if(!sortieRepository.existsById(sortieId))
            throw new EntityNotFoundException(notExsitStart + sortieId + notExsitEnd);

        Date today = (Date) Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());

        if(getSortieById(sortieId).getDate().before(today) || getSortieById(sortieId).getDate().equals(today))
            throw  new IllegalArgumentException("Vous ne pouvez pas supprimer cette sortie car est déjà passé ou meme jour");

        if(getSortieById(sortieId).getDate().before(today))
            throw  new IllegalArgumentException("Vous ne pouvez pas supprimer cette sortie car la date que vous fournissez est antérieure à la date d'aujourd'hui");

        List<ReservationDto> reservationDtoList = reservationService.getAllReservations();
        for(ReservationDto reservationDto : reservationDtoList)
            if(reservationDto.getIdSortie()==sortieId)
                throw  new IllegalArgumentException("Vous ne pouvez pas modifier cette sortie car il y a une/des reservation(s)");

        /*
        Supprimer toutes les options liées à la sortie
         */
        List<OptionDto> optionDtoList = optionService.getAllOptions();
        for (OptionDto optionDto : optionDtoList) {
            if(optionDto.getIdSortie()==sortieId)
                optionService.deleteOption(optionDto.getIdOption());
        }

        sortieRepository.deleteById(sortieId);
        return true;
    }

    /**
     Récupère la liste de toutes les sorties enregistrées dans la base de données.
     @return une liste de SortieDto représentant toutes les sorties dans la base de données.
     */
    @Override
    public List<SortieDto> getAllSorties() {
        return toDto(sortieRepository.findAll());
    }


    /**
     Met à jour une sortie avec les informations fournies.
     @param sortieId ID de la sortie à mettre à jour
     @param sortieDto DTO contenant les informations de la sortie à mettre à jour
     @return SortieDto mise à jour
     @throws IllegalArgumentException si la sortie avec l'ID donné n'existe pas,
     si le DTO de sortie est null, si la date de la sortie est antérieure à la date actuelle,
     si l'heure de début de la sortie est passée ou si la date de la sortie est antérieure à la date d'aujourd'hui,
     si la sortie a des réservations existantes.
     */
    @Override
    public SortieDto updateSortie(int sortieId, SortieDto sortieDto) {
        if(!sortieRepository.existsById(sortieId))
            throw new IllegalArgumentException(notExsitStart + sortieId + notExsitEnd);
        if (sortieDto==null)
            throw  new IllegalArgumentException("La nouvelle sortie est null");
        Date today = (Date) Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        LocalTime localTime = LocalTime.now();
        Time now = Time.valueOf(localTime);

        if(getSortieById(sortieId).getDate().before(today) || getSortieById(sortieId).getDate().equals(today) && now.after(sortieDto.getHeureDebut()))
            throw new IllegalArgumentException("Vous ne pouvez pas modifier cette sortie car elle est déjà passé ou l'heure que vous essayer de mettre est passé");
        if(sortieDto.getDate().before(today))
            throw new IllegalArgumentException("Vous ne pouvez pas modifier cette sortie car la date que vous fournissez est antérieure à la date d'aujourd'hui");
        List<ReservationDto> reservationDtoList = reservationService.getAllReservations();
        for(ReservationDto reservationDto : reservationDtoList)
            if(reservationDto.getIdSortie()==sortieId)
                throw new IllegalArgumentException("Vous ne pouvez pas modifier cette sortie car il y a de(s) reservation(s)");

        SortieDto sortie = getSortieById(sortieId);
        sortie.setNom(sortieDto.getNom());
        sortie.setCapacite(sortieDto.getCapacite());
        sortie.setPrix(sortieDto.getPrix());
        sortie.setDate(sortieDto.getDate());
        sortie.setCapacite(sortieDto.getCapacite());
        sortie.setDescription(sortieDto.getDescription());
        sortie.setHeureDebut(sortieDto.getHeureDebut());
        sortie.setHeureDebut(sortieDto.getHeureDebut());

        return sortie;
    }

    /**
     Convertit un objet SortieDto en un objet Sortie.
     @param dto l'objet SortieDto à convertir.
     @return l'objet Sortie résultant de la conversion.
     */
    public Sortie toEntity(SortieDto dto) {
        Sortie sortie = new Sortie();
        sortie.setIdSortie(dto.getIdSortie());
        sortie.setDescription(dto.getDescription());
        sortie.setNom(dto.getNom());
        sortie.setDate(dto.getDate());
        sortie.setCapacite(dto.getCapacite());
        sortie.setHeureDebut(dto.getHeureDebut());
        sortie.setHeureFin(dto.getHeureFin());
        sortie.setPrix(dto.getPrix());

        return sortie;
    }

    /**
     Convertit un objet Sortie en un objet SortieDto.
     @param entity l'objet Sortie à convertir.
     @return l'objet SortieDto résultant de la conversion.
     */
    public SortieDto toDto(Sortie entity) {
        SortieDto sortieDto = new SortieDto();
        sortieDto.setIdSortie(entity.getIdSortie());
        sortieDto.setDate(entity.getDate());
        sortieDto.setCapacite(entity.getCapacite());
        sortieDto.setHeureDebut(entity.getHeureDebut());
        sortieDto.setHeureFin(entity.getHeureFin());
        sortieDto.setPrix(entity.getPrix());
        sortieDto.setDescription(entity.getDescription());
        sortieDto.setNom(entity.getNom());

        return sortieDto;
    }

    /**
     Convertit une liste d'objets SortieDto en une liste d'objets Sortie.
     @param dtoList la liste d'objets SortieDto à convertir.
     @return la liste d'objets Sortie résultant de la conversion.
     */
    public List<Sortie> toEntity(List<SortieDto> dtoList) {
        if(dtoList.isEmpty())
            return new ArrayList<>();
        List<Sortie> sortieList = new ArrayList<>();
        for(SortieDto sortiedto : dtoList)
            if(sortiedto != null)
                sortieList.add(toEntity(sortiedto));
        return sortieList;
    }

    /**
     Convertit une liste d'objets Sortie en une liste d'objets SortieDto.
     @param entityList la liste d'objets Sortie à convertir.
     @return la liste d'objets SortieDto résultant de la conversion.
     */
    public List<SortieDto> toDto(List<Sortie> entityList) {
        if(entityList.isEmpty())
            return new ArrayList<>();
        List<SortieDto> sortieDtoList = new ArrayList<>();
        for(Sortie sortie : entityList)
            if(sortie!= null)
                sortieDtoList.add(toDto(sortie));
        return sortieDtoList;
    }
}
