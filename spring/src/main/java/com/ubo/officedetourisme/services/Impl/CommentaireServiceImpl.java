package com.ubo.officedetourisme.services.Impl;



import com.ubo.officedetourisme.dto.CommentaireDto;
import com.ubo.officedetourisme.repository.CommentaireRepository;
import com.ubo.officedetourisme.services.CommentaireService;
import com.ubo.officedetourisme.services.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CommentaireService")
public class CommentaireServiceImpl implements CommentaireService {

    @Autowired
    private CommentaireRepository commentaireRepository;

    @Autowired
    private PhotoService photoService;

    /**
     Ajoute un commentaire à une sortie.
     @param commentaireDto le commentaire à ajouter.
     @return le commentaire ajouté.
     @throws IllegalArgumentException si l'utilisateur a déjà ajouté un commentaire pour cette sortie.
     */
    @Override
    public CommentaireDto saveCommentaire(CommentaireDto commentaireDto) {
        List<CommentaireDto> commentaireDtoList = getAllCommentaires();
        for (CommentaireDto commentaire : commentaireDtoList)
            if(commentaire.getIdUtilisateur().equals(commentaireDto.getIdUtilisateur()) && commentaire.getIdSortie().equals(commentaireDto.getIdSortie())  )
                throw new IllegalArgumentException("vous ne pouvez pas ajouter un commentaire car vous avez déja ajouté un");

        return commentaireRepository.save(commentaireDto);
    }

    /**
     Récupère un commentaire par son id.
     @param commentaireId l'id du commentaire à récupérer.
     @return le commentaire correspondant à l'id spécifié ou null s'il n'existe pas.
     */
    @Override
    public CommentaireDto getCommentaireById(String commentaireId) {
        return
                commentaireRepository.findById(commentaireId).orElse(null);
        /*if (commentaireId == null) {
            return null;
        }*/
        //return commentaireRepository.findById(commentaireId).orElse(null);
    }

    /**
     Supprime un commentaire.
     @param commentaireId l'id du commentaire à supprimer.
     @return true si le commentaire a été supprimé, false sinon.
     @throws IllegalArgumentException si le commentaire avec l'id spécifié n'existe pas.
     */
    @Override
    public boolean deleteCommentaire(String commentaireId) {

        if(!commentaireRepository.existsById(commentaireId))
            throw new IllegalArgumentException("le commentaire avec l'id :" + commentaireId + " n'existe pas");

        commentaireRepository.deleteById(commentaireId);
        return true;

    }
    /**
     Récupère tous les commentaires.
     @return une liste de tous les commentaires.
     */
    @Override
    public List<CommentaireDto> getAllCommentaires() {
        return commentaireRepository.findAll();
    }

    /**
     Met à jour un commentaire.
     @param commentaireId l'id du commentaire à mettre à jour.
     @param commentaireDto les nouvelles informations à mettre à jour.
     @return le commentaire mis à jour.
     @throws IllegalArgumentException si le commentaire avec l'id spécifié n'existe pas.
     */
    @Override
    public CommentaireDto updateCommentaire(String commentaireId, CommentaireDto commentaireDto) {
       CommentaireDto existingCommentaire = commentaireRepository.findById(commentaireId).orElse(null);
        if (existingCommentaire == null) {
            throw new IllegalArgumentException("Le commentaire avec l'id : " + commentaireId + " n'existe pas.");
        }
        existingCommentaire.setText(commentaireDto.getText());
        return commentaireRepository.save(commentaireDto);
    }
}

