package com.ubo.officedetourisme.services.Impl;

import com.ubo.officedetourisme.dto.OptionDto;
import com.ubo.officedetourisme.entities.Option;
import com.ubo.officedetourisme.entities.Sortie;
import com.ubo.officedetourisme.repository.OptionRepository;
import com.ubo.officedetourisme.services.OptionService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 *
 * @author Abderraouf
 */
@Service("OptionService")
/**
 Cette classe est une implémentation de l'interface OptionService qui gère les opérations sur les options.
 */
public class OptionServiceImpl implements OptionService {

    private final OptionRepository optionRepository;

    public OptionServiceImpl(OptionRepository optionRepository) {
        this.optionRepository = optionRepository;
    }

    /**
     Enregistre une option dans la base de données.
     @param optionDto l'option à enregistrer
     @return l'option enregistrée sous forme d'OptionDto
     */
    @Override
    public OptionDto saveOption(OptionDto optionDto) {
        return toDto(optionRepository.save(toEntity(optionDto)));
    }

    /**
     Retourne l'option correspondante à l'id passé en paramètre.
     @param optionId identifiant de l'option à récupérer.
     @return l'objet OptionDto correspondant à l'option trouvée.
     @throws EntityNotFoundException si l'option avec l'id spécifié n'existe pas.
     */
    @Override
    public OptionDto getOptionById(int optionId) {
        Optional<Option> option = optionRepository.findById(optionId);
        if(option.isPresent())
            return toDto(option.get());
        throw new EntityNotFoundException("la reservation avec l'id :" + optionId + " n'existe pas");

    }

    /**
     Supprime l'option correspondant à l'id passé en paramètre.
     @param optionId identifiant de l'option à supprimer.
     @return true si l'option a été supprimée, false sinon.
     */
    @Override
    public boolean deleteOption(int optionId) {
        optionRepository.deleteById(optionId);
        return true;
    }

    /**
     Retourne une liste contenant toutes les options.
     @return une liste d'objets OptionDto correspondant à toutes les options trouvées.
     */
    @Override
    public List<OptionDto> getAllOptions() {
        List<OptionDto> optionDtoList = new ArrayList<>();
        List<Option> dogs = optionRepository.findAll();
        dogs.forEach(option -> optionDtoList.add(toDto(option)));

        return optionDtoList;
    }

    /**
     Met à jour l'option correspondant à l'id passé en paramètre avec les nouvelles données fournies dans l'objet OptionDto.
     @param optionId identifiant de l'option à mettre à jour.
     @param optionDto objet OptionDto contenant les nouvelles données de l'option.
     @return l'objet OptionDto mis à jour.
     @throws IllegalArgumentException si l'option avec l'id spécifié n'existe pas.
     */
    @Override
    public OptionDto updateOption(int optionId, OptionDto optionDto) {
        if(!optionRepository.existsById(optionId))
            throw new IllegalArgumentException("l'option avec l'id :" + optionId + " n'existe pas");
        OptionDto optionDto1 = getOptionById(optionId);
        optionDto1.setDescription(optionDto.getDescription());
        optionDto1.setNom(optionDto.getNom());

        return saveOption(optionDto1);
    }

    /**
     Transforme un objet OptionDto en objet Option.
     @param optionDto objet OptionDto à transformer.
     @return l'objet Option correspondant.
     */
    public Option toEntity(OptionDto optionDto) {
        Option option = new Option();

        option.setIdOption(optionDto.getIdOption());
        option.setNom(optionDto.getNom());
        option.setDescription(optionDto.getDescription());

        Sortie sortie = new Sortie();
        sortie.setIdSortie(optionDto.getIdSortie());
        option.setIdSortie(sortie);

        return option;
    }

    /**
     Convertit une entité Option en un objet OptionDto.
     @param option l'entité Option à convertir
     @return l'objet OptionDto converti
     */
    public OptionDto toDto(Option option) {

        OptionDto optionDto = new OptionDto();
        optionDto.setIdOption(option.getIdOption());
        optionDto.setIdSortie(option.getIdSortie().getIdSortie());
        optionDto.setDescription(option.getDescription());
        optionDto.setNom(option.getNom());

        return  optionDto;
    }

    /**
     Convertit une liste d'objets OptionDto en une liste d'entités Option.
     @param dtoList la liste d'objets OptionDto à convertir
     @return la liste d'entités Option convertie
     */
    public List<Option> toEntity(List<OptionDto> dtoList) {
        if (dtoList.isEmpty()) {
            return new ArrayList<>();
        }
        List<Option> optionList = new ArrayList<>();
        for( OptionDto optionDto : dtoList)
            if(optionDto != null)
                optionList.add(toEntity(optionDto));
        return optionList;
    }

    /**
     Convertit une liste d'entités Option en une liste d'objets OptionDto.
     @param entityList la liste d'entités Option à convertir
     @return la liste d'objets OptionDto convertie
     */
    public List<OptionDto> toDto(List<Option> entityList) {
        if (entityList.isEmpty()) {
            return new ArrayList<>();
        }
        List<OptionDto> optionDtoList = new ArrayList<>();
        for (Option option : entityList)
            if(option != null)
                optionDtoList.add(toDto(option));
        return optionDtoList;
    }
}
