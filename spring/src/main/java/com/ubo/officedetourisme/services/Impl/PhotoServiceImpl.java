package com.ubo.officedetourisme.services.Impl;


import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.ubo.officedetourisme.dto.Photo;
import com.ubo.officedetourisme.services.PhotoService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class PhotoServiceImpl implements PhotoService {

    @Autowired
    private GridFsTemplate template;

    @Autowired
    private GridFsOperations operations;

    /**
     Ajoute un fichier photo associé à un utilisateur et à une sortie donnée.
     @param upload le fichier à ajouter
     @param idSortie l'identifiant de la sortie associée au fichier
     @param idUtilisateur l'identifiant de l'utilisateur associé au fichier
     @return l'identifiant du fichier ajouté
     @throws IOException si une erreur survient lors de la lecture ou de la copie du fichier
     */
    public String addFile(MultipartFile upload, String idSortie, String idUtilisateur) throws IOException {

        DBObject metadata = new BasicDBObject();
        metadata.put("fileSize", upload.getSize());
        metadata.put("idSortie", idSortie);
        metadata.put("idUtilisateur", idUtilisateur);
        Object fileID = template.store(upload.getInputStream(), upload.getOriginalFilename(), upload.getContentType(), metadata);

        return fileID.toString();
    }
    /**
     Télécharge un fichier photo associé à un utilisateur et à une sortie donnée.
     @param idUtilisateur l'identifiant de l'utilisateur associé au fichier
     @param idSortie l'identifiant de la sortie associée au fichier
     @return un objet Photo contenant les informations du fichier téléchargé
     @throws IOException si une erreur survient lors de la lecture ou de la copie du fichier
     */

    public Photo downloadFile(String idUtilisateur, String idSortie) throws IOException {

        Query query = new Query(Criteria.where("metadata.idUtilisateur").is(idUtilisateur)
                .and("metadata.idSortie").is(idSortie));

        GridFSFile gridFSFile = template.findOne(query);

        Photo loadFile = new Photo();

        if (gridFSFile != null && gridFSFile.getMetadata() != null) {
            loadFile.setFilename( gridFSFile.getFilename() );
            loadFile.setFileType( gridFSFile.getMetadata().get("_contentType").toString() );
            loadFile.setFileSize( gridFSFile.getMetadata().get("fileSize").toString() );
            loadFile.setFile( IOUtils.toByteArray(operations.getResource(gridFSFile).getInputStream()) );
        }

        return loadFile;
    }



}
