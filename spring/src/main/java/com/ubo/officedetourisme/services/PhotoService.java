package com.ubo.officedetourisme.services;

import com.ubo.officedetourisme.dto.Photo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
/**
 *
 * @author Abderraouf
 */
public interface PhotoService {

    /**
     Ajoute un fichier photo associé à un utilisateur et à une sortie donnée.
     @param upload le fichier à ajouter
     @param idSortie l'identifiant de la sortie associée au fichier
     @param idUtilisateur l'identifiant de l'utilisateur associé au fichier
     @return l'identifiant du fichier ajouté
     @throws IOException si une erreur survient lors de la lecture ou de la copie du fichier
     */
    String addFile(MultipartFile upload, String idSortie, String idUtilisateur) throws IOException;



    /**
     Télécharge un fichier photo associé à un utilisateur et à une sortie donnée.
     @param idUtilisateur l'identifiant de l'utilisateur associé au fichier
     @param idSortie l'identifiant de la sortie associée au fichier
     @return un objet Photo contenant les informations du fichier téléchargé
     @throws IOException si une erreur survient lors de la lecture ou de la copie du fichier
     */
    Photo downloadFile(String idUtilisateur, String idSortie) throws IOException;
}
