package com.ubo.officedetourisme.services.Impl;

import com.ubo.officedetourisme.dto.LoginRequest;
import com.ubo.officedetourisme.dto.ReservationDto;
import com.ubo.officedetourisme.dto.UtilisateurDto;
import com.ubo.officedetourisme.entities.Utilisateur;
import com.ubo.officedetourisme.repository.UtilisateurRepository;
import com.ubo.officedetourisme.services.UtilisateurService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Classe qui implémente les services d'un utilisateur.
 *
 * @author Abderraouf
 */

@Service("UtilisateurService")
public class UtilisateurServiceImpl implements UtilisateurService {

    private  final UtilisateurRepository utilisateurRepository;
    private  final ReservationServiceImpl reservationService;
    String notExsitStart = "l'utilisateur avec l'id :" ;
    String notExsitEnd = " n'existe pas" ;

    /**
     Constructeur de la classe UtilisateurServiceImpl.
     @param utilisateurRepository le repository utilisé pour accéder aux données de l'utilisateur
     @param reservationService le service utilisé pour les réservations de l'utilisateur
     */
    public UtilisateurServiceImpl( UtilisateurRepository utilisateurRepository, ReservationServiceImpl reservationService) {
        this.utilisateurRepository = utilisateurRepository;
        this.reservationService = reservationService;
    }
    /**
     Calcule le hash d'un mot de passe en utilisant l'algorithme de hachage SHA-256.
     @param password mot de passe à hasher.
     @return le mot de passe hashé.
     @throws NoSuchAlgorithmException si l'algorithme de hashage est introuvable.
     */


    public static String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] hashedPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (byte b : hashedPassword) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
    /**
     Enregistre un nouvel utilisateur dans la base de données.
     @param utilisateurDto instance de l'utilisateur à sauvegarder.
     @return l'instance de l'utilisateur sauvegardé.
     @throws NoSuchAlgorithmException si l'algorithme de hashage est introuvable.
     @throws IllegalArgumentException si l'utilisateur existe déjà.
     */
    @Override
    public UtilisateurDto saveUtilisateur(UtilisateurDto utilisateurDto) throws NoSuchAlgorithmException {
        List<UtilisateurDto> utilisateurList = getAllUtilisateurs();
        for (UtilisateurDto utilisateurDto1 : utilisateurList)
            if(utilisateurDto1.getLogin().equals(utilisateurDto.getLogin()) ||
                    utilisateurDto1.getAdresseMail().equals(utilisateurDto.getAdresse()))
                throw new IllegalArgumentException("Vous ne pouvez pas utilisé ce login/mail");
        utilisateurDto.setMotDePasse(hashPassword(utilisateurDto.getMotDePasse()));
        return toDto(utilisateurRepository.save(toEntity(utilisateurDto)));
    }

    /**
     @param utilisateurId ID de l'utilisateur à récupérer.
     @return l'instance de l'utilisateur correspondant à l'ID.
     @throws EntityNotFoundException si l'utilisateur n'existe pas.
     */
    @Override
    public UtilisateurDto getUtilisateurById(int utilisateurId) {
        Optional<Utilisateur> utilisateur = utilisateurRepository.findById(utilisateurId);

        if(utilisateur.isPresent())
            return toDto(utilisateur.get());
        throw new EntityNotFoundException(notExsitStart + utilisateurId + notExsitEnd);

    }

    /**
     @param utilisateurId ID de l'utilisateur à supprimer.
     @return true si l'utilisateur a été supprimé avec succès, false sinon.
     @throws EntityNotFoundException si l'utilisateur n'existe pas.
     */
    @Override
    public boolean deleteUtilisateur(int utilisateurId) {
        if(!utilisateurRepository.existsById(utilisateurId))
            throw new EntityNotFoundException(notExsitStart + utilisateurId + notExsitEnd);
        // supprimer toutes les reservations lié à l'utilisateur
        List<ReservationDto> reservationDtoList = reservationService.getAllReservations();
        for (ReservationDto reservationDto : reservationDtoList)
            if(reservationDto.getIdUtilisateur()==utilisateurId)
                reservationService.deleteReservation(reservationDto.getIdReservation());
        //Puis supprimer l'utilisateur
        utilisateurRepository.deleteById(utilisateurId);
        return true;
    }

    /**
     Récupère la liste de tous les utilisateurs enregistrés dans la base de données.
     @return une liste de tous les utilisateurs enregistrés dans la base de données.
     */
    @Override
    public List<UtilisateurDto> getAllUtilisateurs() {
        return toDto(utilisateurRepository.findAll());
    }

    /**

     Met à jour un utilisateur avec les informations fournies.
     @param utilisateurId l'ID de l'utilisateur à mettre à jour
     @param utilisateurDto les informations de l'utilisateur mises à jour
     @return l'utilisateur mis à jour sous forme d'objet UtilisateurDto
     @throws NoSuchAlgorithmException si le chiffrement de mot de passe échoue
     @throws EntityNotFoundException si l'utilisateur avec l'ID donné n'existe pas
     */
    @Override
    public UtilisateurDto updateUtilisateur(int utilisateurId, UtilisateurDto utilisateurDto) throws NoSuchAlgorithmException {
        if(!utilisateurRepository.existsById(utilisateurId))
            throw new EntityNotFoundException(notExsitStart + utilisateurId + notExsitEnd);

        UtilisateurDto  utilisateurDto1 = getUtilisateurById(utilisateurId);
        utilisateurDto1.setMotDePasse(hashPassword(utilisateurDto.getMotDePasse()));
        utilisateurDto1.setNom(utilisateurDto.getNom());
        utilisateurDto1.setPrenom(utilisateurDto.getPrenom());
        utilisateurDto1.setVille(utilisateurDto.getVille());
        utilisateurDto1.setGenre(utilisateurDto.getGenre());
        utilisateurDto1.setEstAdministrateur(utilisateurDto.isEstAdministrateur());
        utilisateurDto1.setAdresseMail(utilisateurDto.getAdresseMail());
        utilisateurDto1.setAdresse(utilisateurDto.getAdresse());
        utilisateurDto1.setLogin(utilisateurDto.getLogin());
        utilisateurDto1.setLogin(utilisateurDto.getLogin());

        return toDto(utilisateurRepository.save(toEntity(utilisateurDto1)));
    }

    /**
     Convertit un objet UtilisateurDto en objet Utilisateur.
     @param dto l'objet UtilisateurDto à convertir
     @return l'objet Utilisateur converti
     */
    public Utilisateur toEntity(UtilisateurDto dto) {

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(dto.getIdUtilisateur());
        utilisateur.setGenre(dto.getGenre());
        utilisateur.setNom(dto.getNom());
        utilisateur.setPrenom(dto.getPrenom());
        utilisateur.setLogin(dto.getLogin());
        utilisateur.setMotDePasse(dto.getMotDePasse());
        utilisateur.setAdresseMail(dto.getAdresseMail());
        utilisateur.setAdresse(dto.getAdresse());
        utilisateur.setVille(dto.getVille());
        utilisateur.setEstAdministrateur(dto.isEstAdministrateur());

        return utilisateur;
    }

    /**
     Convertit un objet Utilisateur en objet UtilisateurDto.
     @param utilisateur l'objet Utilisateur à convertir
     @return l'objet UtilisateurDto converti
     */
    public UtilisateurDto toDto(Utilisateur utilisateur) {

        UtilisateurDto utilisateurDto = new UtilisateurDto();
        utilisateurDto.setIdUtilisateur(utilisateur.getIdUtilisateur());
        utilisateurDto.setGenre(utilisateur.getGenre());
        utilisateurDto.setNom(utilisateur.getNom());
        utilisateurDto.setPrenom(utilisateur.getPrenom());
        utilisateurDto.setLogin(utilisateur.getLogin());
        utilisateurDto.setMotDePasse(utilisateur.getMotDePasse());
        utilisateurDto.setAdresseMail(utilisateur.getAdresseMail());
        utilisateurDto.setAdresse(utilisateur.getAdresse());
        utilisateurDto.setVille(utilisateur.getVille());
        utilisateurDto.setEstAdministrateur(utilisateur.isEstAdministrateur());

        return utilisateurDto;
    }
    /**
     Convertit une liste d'objets UtilisateurDto en une liste d'objets Utilisateur.
     @param dtoList la liste d'objets UtilisateurDto à convertir
     @return la liste d'objets Utilisateur convertie
     */
    public List<Utilisateur> toEntity(List<UtilisateurDto> dtoList) {
        if(dtoList.isEmpty()) return new ArrayList<>();
        List<Utilisateur> utilisateurList = new ArrayList<>();
        for (UtilisateurDto utilisateurDto : dtoList)
            if(utilisateurDto != null)
                utilisateurList.add(toEntity(utilisateurDto));

        return  utilisateurList;
    }

    /**
     Convertit une liste d'objets Utilisateur en une liste d'objets UtilisateurDto.
     @param entityList la liste d'objets Utilisateur à convertir
     @return la liste d'objets UtilisateurDto convertie
     */
    public List<UtilisateurDto> toDto(List<Utilisateur> entityList) {
        if(entityList.isEmpty())
            return new ArrayList<>();
        List<UtilisateurDto> utilisateurList = new ArrayList<>();
        for (Utilisateur utilisateur : entityList)
            if(utilisateur != null)
                utilisateurList.add(toDto(utilisateur));
        return  utilisateurList;
    }

    /**
     Connecte un utilisateur en vérifiant son login et mot de passe.
     @param loginRequest la requête de connexion contenant le login et le mot de passe
     @return l'objet UtilisateurDto de l'utilisateur connecté, null si la connexion échoue
     @throws NoSuchAlgorithmException si le chiffrement du mot de passe échoue
     */
    public UtilisateurDto loginUtilisateur(LoginRequest loginRequest) throws NoSuchAlgorithmException {
        Utilisateur utilisateur = utilisateurRepository.findByLogin(loginRequest.getLogin());
        if(utilisateur.getLogin().equals(loginRequest.getLogin()) &&
                utilisateur.getMotDePasse().equals(hashPassword(loginRequest.getMotDePasse()))) {
            return toDto(utilisateur);
        } else {
            return null;
        }
    }
}
