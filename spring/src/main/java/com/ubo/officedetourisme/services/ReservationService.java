package com.ubo.officedetourisme.services;

import com.ubo.officedetourisme.dto.ReservationDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author Abderraouf
 */
@Service
public interface ReservationService {

    /**
     * Enregistre un nouvel objet ReservationDto.
     * @param reservationDto L'objet ReservationDto à enregistrer.
     * @return L'objet ReservationDto enregistré.
     */
    ReservationDto saveReservation(ReservationDto reservationDto);

    /**
     * Récupère un objet ReservationDto par son ID.
     * @param reservationId L'ID de l'objet ReservationDto à récupérer.
     * @return L'objet ReservationDto récupéré.
     */
    ReservationDto getReservationById(int reservationId);

    /**
     * Supprime un objet ReservationDto par son ID.
     * @param reservationId L'ID de l'objet ReservationDto à supprimer.
     * @return Vrai si l'objet ReservationDto a été supprimé avec succès, faux sinon.
     */
    boolean deleteReservation(int reservationId);

    /**
     * Récupère une liste de tous les objets ReservationDto.
     * @return La liste des objets ReservationDto.
     */
    List<ReservationDto> getAllReservations();

    /**
     * Met à jour un objet ReservationDto par son ID.
     * @param reservationId L'ID de l'objet ReservationDto à mettre à jour.
     * @param reservationDto L'objet ReservationDto mis à jour.
     * @return L'objet ReservationDto mis à jour.
     */
    ReservationDto updateReservation(int reservationId, ReservationDto reservationDto);

}
