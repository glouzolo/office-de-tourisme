package com.ubo.officedetourisme.services;

import com.ubo.officedetourisme.dto.LoginRequest;
import com.ubo.officedetourisme.dto.UtilisateurDto;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
/**
 *
 * @author Abderraouf
 */
@Service
public interface UtilisateurService {

    /**
     * Enregistre un nouvel objet UtilisateurDto.
     * @param utilisateurDto L'objet UtilisateurDto à enregistrer.
     * @return L'objet UtilisateurDto enregistré.
     * @throws NoSuchAlgorithmException si le chiffrement de mot de passe échoue
     */
    UtilisateurDto saveUtilisateur(UtilisateurDto utilisateurDto) throws NoSuchAlgorithmException;

    /**
     * Récupère un objet UtilisateurDto par son ID.
     * @param utilisateurId L'ID de l'objet UtilisateurDto à récupérer.
     * @return L'objet UtilisateurDto récupéré.
     */
    UtilisateurDto getUtilisateurById(int utilisateurId);

    /**
     * Supprime un objet UtilisateurDto par son ID.
     * @param utilisateurId l'ID de l'utilisateur à supprimer
     * @return Vrai si l'objet UtilisateurDto a été supprimé avec succès, faux sinon.
     */
    boolean deleteUtilisateur(int utilisateurId);

    /**
     * Récupère une liste de tous les objets UtilisateurDto.
     * @return La liste des objets UtilisateurDto.
     */
    List<UtilisateurDto> getAllUtilisateurs();

    /**
     * Met à jour un objet SalleDto par son ID.
     * @param utilisateurId L'ID de l'objet SalleDto à mettre à jour.
     * @param utilisateurDto L'objet SalleDto mis à jour.
     * @return L'objet UtilisateurDto mis à jour.
     * @throws NoSuchAlgorithmException si le chiffrement de mot de passe échoue
     */
    UtilisateurDto updateUtilisateur(int utilisateurId, UtilisateurDto utilisateurDto) throws NoSuchAlgorithmException;

    /**
     * Récupère un objet UtilisateurDto par son login.
     * @param loginRequest  de l'objet UtilisateurDto à récupérer.
     * @return L'objet UtilisateurDto récupéré.
     * @throws NoSuchAlgorithmException si l'algorithme de hashage est introuvable.
     */
    UtilisateurDto loginUtilisateur(LoginRequest loginRequest) throws NoSuchAlgorithmException;


}
