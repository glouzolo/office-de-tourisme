package com.ubo.officedetourisme.dto;


import lombok.Data;

/**
 * DTO représentant une option.
 * @author Abderraouf
 */
@Data
public class OptionDto  {
    private int idOption;
    private String nom;
    private String description;
    private int idSortie;
}