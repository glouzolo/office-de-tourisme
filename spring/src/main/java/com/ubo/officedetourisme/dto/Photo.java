package com.ubo.officedetourisme.dto;

import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * DTO représentant une photo.
 * @author Abderraouf
 */
@AllArgsConstructor
@Document(collection = "photos")
public class Photo {

    private String filename;
    private String fileType;
    private String fileSize;
    private byte[] file;
    private String idSortie;
    private String idUtilisateur;

    public Photo() {
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getIdSortie() {
        return idSortie;
    }

    public void setIdSortie(String idSortie) {
        this.idSortie = idSortie;
    }

    public String getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(String idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

}
