package com.ubo.officedetourisme.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * DTO représentant un commentaire.
 * @author Abderraouf
 */
@Data
@Document("Commentaire")
public class CommentaireDto {

    @Id
    private String idCommentaire;
    private String date;
    private String heure;
    private String text;
    private String idSortie;
    private String idUtilisateur;
}
