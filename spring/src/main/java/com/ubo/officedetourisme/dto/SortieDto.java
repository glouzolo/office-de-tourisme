package com.ubo.officedetourisme.dto;


import lombok.Data;
import java.sql.Date;
import java.sql.Time;

/**
 * DTO représentant une sortie.
 * @author Abderraouf
 */
@Data
public class SortieDto {
    private int idSortie;
    private String nom;
    private Date date;
    private Time heureDebut;
    private Time heureFin;
    private int capacite;
    private double prix;
    private String description;
}