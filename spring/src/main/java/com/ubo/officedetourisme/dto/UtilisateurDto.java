package com.ubo.officedetourisme.dto;

import lombok.Data;

/**
 * Class représentant un utilisateurDto.
 * @author Abderraouf
 */


@Data
/**
 * DTO représentant un utilisateur.
 */
public class UtilisateurDto  {
    private int idUtilisateur;
    private String genre;
    private String nom;
    private String prenom;
    private String login;
    private String motDePasse;
    private String adresseMail;
    private String adresse;
    private String ville;
    private boolean estAdministrateur;
}