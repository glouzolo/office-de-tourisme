package com.ubo.officedetourisme.dto;

import lombok.Data;
/**
 * DTO représentant une connexion.
 * @author Abderraouf
 */
@Data
public class LoginRequest {
     private String login;
     private String motDePasse;
}
