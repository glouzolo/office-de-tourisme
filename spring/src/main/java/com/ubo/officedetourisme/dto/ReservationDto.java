package com.ubo.officedetourisme.dto;

import lombok.Data;
import java.sql.Date;
import java.sql.Time;

/**
 * DTO représentant une réservation.
 * @author Abderraouf
 */
@Data
public class ReservationDto {
    private int idReservation;
    private int idSortie;
    private int idUtilisateur;
    private Integer idOption; // use Integer to handle null value
    private Date date;
    private Time heure;
    private boolean estDansPanier;
}